import Server from "../src/Server";
import chai from 'chai';
import chaiHttp from 'chai-http';
import ConfigModel from "../src/models/ConfigModel";
let expect = chai.expect;

chai.use(chaiHttp);

describe('Admin tests', function(){
    let token:string;
    describe('/POST login', function() {
        it('should  return a jwt token', function (done:any) {
            chai.request('http://localhost:' + Server.port)
                .post('/login')
                .send({
                    "mail": "admin@mail.com",
                    "password": "pass"
                })
                .end((error:any, res) => {
                    if(res.body.error) {
                        console.log(res.body);
                    }
                    token = res.body.token;
                    expect(res).to.have.status(200);
                    expect(res.body).to.have.property('token');
                    expect(res.body.token).to.be.a('string');
                    done();
                });
        });
    });


    describe('/POST add_user', function() {
        it('should  add a user in the databse and return a success message', function (done:any) {
            chai.request('http://localhost:' + Server.port)
                .post('/add_user')
                .send({
                    "mail": "user2@mail.com",
                    "password": "pass",
                    "firstName" : "Quentin",
                    "lastName" : "Vevaud"
                })
                .set({'Authorization':`Bearer ${token}`})
                .end((error:any, res) => {
                    if(res.body.error) {
                        console.log(res.body);
                    }
                    expect(res).to.have.status(201);
                    expect(res.body).to.have.property('message');
                    expect(res.body.message).to.be.a('string');
                    done();
                });
        });
    });


    describe('/POST change_price', function() {
        it('should change the price of a subscription and return a success message', function (done:any) {
            chai.request('http://localhost:' + Server.port)
                .post('/change_price')
                .send({
                    "price":"12.99",
                    "isAnnual":"true"
                })
                .set({'Authorization':`Bearer ${token}`})
                .end((error:any, res) => {
                    if(res.body.error) {
                        console.log(res.body);
                    }
                    expect(res).to.have.status(200);
                    expect(res.body).to.have.property('message');
                    expect(res.body.message).to.be.a('string');
                    done();
                });
        });
    });


    describe.skip('/POST change_mail', function () {
        it("Should change the user's mail", function (done:any) {
            chai.request('http://localhost:' + Server.port)
                .post('/change_mail')
                .send({
                    "mail" : "admin@mail.com",
                    "newMail":"admin@gmail.com"
                })
                .set({'Authorization':`Bearer ${token}`})
                .end(function (error, res) {
                    if(res.body.error) {
                        console.log(res.body);
                    }
                    expect(res).to.have.status(200);
                    expect(res.body).to.have.property('message');
                    done();
                })
        })
    })

    describe('/GET logout', function() {
            it('Should return status code 200', function (done:any) {
                chai.request('http://localhost:' + Server.port)
                    .get('/logout')
                    .set({'Authorization':`Bearer ${token}`})
                    .end(function (error:any, res) {
                        if(res.body.error) {
                            console.log(res.body);
                        }
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('message');
                        done();
                    })
            })
        }
    )
})