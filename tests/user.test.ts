import Server from "../src/Server";
import chai from 'chai';
import chaiHttp from 'chai-http';
import ConfigModel from "../src/models/ConfigModel";
let expect = chai.expect;


chai.use(chaiHttp);
describe('User tests', function() {
    let token:string;
    describe('/POST login', function() {
        it('should  return a jwt token', function (done:any) {
            chai.request('http://localhost:' + Server.port)
                .post('/login')
                .send({
                    "mail": "user@mail.com",
                    "password": "pass"
                })
                .end((error:any, res) => {
                    if(res.body.error) {
                        console.log(res.body);
                    }
                    token = res.body.token;
                    expect(res).to.have.status(200);
                    expect(res.body).to.have.property('token');
                    expect(res.body.token).to.be.a('string');
                    done();
                });
        });
    });


    describe('/GET profile', function() {
            it("Should return the user's profile's informations", function (done:any) {
                chai.request('http://localhost:' + Server.port)
                    .get('/profile')
                    .set({'Authorization':`Bearer ${token}`})
                    .end(function (error:any, res) {
                        if(res.body.error) {
                            console.log(res.body);
                        }
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('mail');
                        expect(res.body).to.have.property('firstName');
                        expect(res.body).to.have.property('lastName');
                        expect(res.body).to.have.property('subscription');
                        expect(res.body.mail).to.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
                        done();
                    })
            })
        }
    )

    describe.skip('/POST change_mail', function () {
        it("Should change the user's mail", function (done:any) {
            chai.request('http://localhost:' + Server.port)
                .post('/change_mail')
                .send({
                    "mail" : "user@mail.com",
                    "newMail":"usere@mail.com"
                })
                .set({'Authorization':`Bearer ${token}`})
                .end(function (error, res) {
                    if(res.body.error) {
                        console.log(res.body);
                    }
                    expect(res).to.have.status(200);
                    expect(res.body).to.have.property('message');
                    done();
                })
        })
    })


    describe('/GET show_subscriptions', function () {
        it("Should show the subscriptions available and their price", function (done:any) {
            chai.request('http://localhost:' + Server.port)
                .get('/show_subscriptions')
                .set({'Authorization':`Bearer ${token}`})
                .end(function (error:any, res) {
                    if(res.body.error) {
                        console.log(res.body);
                    }
                    expect(res).to.have.status(200);
                    expect(res.body[0]).to.have.property('title', 'Monthly subscription');
                    expect(res.body[0]).to.have.property('price');
                    expect(res.body[1]).to.have.property('title', 'Annual subscription');
                    expect(res.body[0].price).to.be.a('number');
                    expect(res.body[1].price).to.be.a('number');
                    done();
                });
        });
    });


    describe('/POST add_to_cart', function () {
        it("Should add to the cart a subscription", function (done:any) {
            chai.request('http://localhost:' + Server.port)
                .post('/add_to_cart')
                .set({'Authorization':`Bearer ${token}`})
                .send({"subscription":"monthly"})
                .end(function (error:any, res) {
                    if(res.body.error) {
                        console.log(res.body);
                    }
                    expect(res).to.have.status(201);
                    expect(res.body).to.have.property('message');
                    done();
                });
        });
    });


    describe('/GET show_cart', function () {
        it("Should show what is in the cart", function (done:any) {
            chai.request('http://localhost:' + Server.port)
                .get('/show_cart')
                .set({'Authorization':`Bearer ${token}`})
                .end(function (error:any, res) {
                    if(res.body.error) {
                        console.log(res.body);
                    }
                    expect(res).to.have.status(200);
                    expect(res.body).to.have.property('message');
                    expect(res.body.message).to.be.a('string');
                    done();
                });
        });
    });


    describe('/POST checkout', function () {
        it("Should create a new subscription and return a seccess message", function (done:any) {
            chai.request('http://localhost:' + Server.port)
                .post('/checkout')
                .set({'Authorization':`Bearer ${token}`})
                .send({
                    "cardNumber" : "0190293473492394",
                    "name" : "Quentin Vevaud",
                    "expirationDate" : "2020/09",
                    "code":"754"
                })
                .end(function (error:any, res) {
                    if(res.body.error) {
                        console.log(res.body);
                    }
                    expect(res).to.have.status(201);
                    expect(res.body).to.have.property('message');
                    expect(res.body).to.have.property('price');
                    expect(res.body.price).to.be.a('number');
                    expect(res.body.message).to.be.a('string');
                    done();
                });
        });
    });

    describe('/GET logout', function() {
            it('Should return status code 200', function (done:any) {
                chai.request('http://localhost:' + Server.port)
                    .get('/logout')
                    .set({'Authorization':`Bearer ${token}`})
                    .end(function (error:any, res) {
                        if(res.body.error) {
                            console.log(res.body);
                        }
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('message');
                        done();
                    })
            })
        }
    )
})
