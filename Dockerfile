FROM node:current-slim

ADD package.json /API-shadow/

WORKDIR /API-shadow/

RUN npm install

ADD . /API-shadow/

CMD npm start