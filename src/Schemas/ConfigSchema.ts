const mongoose = require('mongoose');

const ConfigSchema = mongoose.model('ConfigSchema', new mongoose.Schema({
    annualSubscriptionPrice: {type: Number, required: true,},
    monthlySubscriptionPrice: {type: Number, required: true},
    tokenBlackList : [String]
}));

export default ConfigSchema;
