const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const AdminSchema = mongoose.model('AdminSchema', new mongoose.Schema({
    mail: {type: String, required: true, unique:true},
    password: {type: String, required: true}
}).plugin(uniqueValidator));

export default AdminSchema;
