const mongoose = require('mongoose');
const ttl = require('mongoose-ttl');

const SubscriptionSchema = mongoose.model('SubscriptionSchema', new mongoose.Schema({
    subscription : {type : String, required : true},
    price: {type: Number, required: true},
    idUser: {type: String, required: true},
}).plugin(ttl));

export default SubscriptionSchema;
