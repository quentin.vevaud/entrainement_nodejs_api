const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const UserSchema = mongoose.model('UserSchema', new mongoose.Schema({
    mail: {type: String, required: true, unique:true},
    password: {type: String, required: true},
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    cart: {type: String}
}).plugin(uniqueValidator));

export default UserSchema;

