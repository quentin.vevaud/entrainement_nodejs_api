import {app} from "./Server";
import DatabaseConnection from "./models/databaseConnection";
import {Request, Response} from "express";
import Server from "./Server";
import bodyParser from 'body-parser';
import HostController from './controllers/HostController';
import UserController from "./controllers/UserController";
import AdminController from "./controllers/AdminController";
import bcrypt from 'bcrypt';
import AdminModel from "./models/AdminModel";

new Server();
new DatabaseConnection();

app.use(bodyParser.json())
//Log in the user
app.post('/login', HostController.login)

//Log out the user
app.get('/logout', HostController.auth, HostController.logout)

//Show profile informations
app.get('/profile', HostController.auth, UserController.profile)

// Change the account's mail
app.post('/change_mail', HostController.auth, HostController.changeMail)

// Add a subscription to the cart
app.post('/add_to_cart', HostController.auth, UserController.addToCart)

// Show the user's cart
app.get('/show_cart', HostController.auth, UserController.showCart)

// Show the subscriptions available
app.get('/show_subscriptions', HostController.showSubscriptions)

// Checkout cart
app.post('/checkout', HostController.auth,  UserController.checkout)

// Change subscription price (admin)
app.post('/change_price', HostController.auth, AdminController.changePrice)

// Add a user (admin)
app.post('/add_user', HostController.auth, AdminController.addUser)

// Create an admin (temporary)
// app.post('/add_admin', bodyParser.json(), function (req:Request,res:Response){
//     const mail:string = String(req.body.mail);
//     const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//     if(!regex.test(req.body.mail)) {
//         return res.status(403).json({error:'The format of this mail address is invalid'})
//     }
//     if(req.body.password == undefined) {
//         return res.status(403).json({error:'Enter a password please'})
//     }
//     HostController.mailAlreadyExist(mail).then(function (boolean:any) {
//         if(!boolean) {
//             bcrypt.hash(req.body.password, 10)
//                 .then(function (password: string) {
//                     AdminModel.createAdmin(mail, password)
//                         .then(function () {
//                             res.status(201).json({message: 'Administrator added'});
//                         })
//                         .catch(function (error: any) {
//                             res.status(500).json({error:error.message});
//                         });
//                 })
//                 .catch(function (error: any) {
//                     res.status(500).json({error:error.message});
//                 });
//         } else {
//             res.status(400).json({error: 'This mail address is already used'})
//         }
//     })
// })

// Error 404 if no route corresponding
app.use(function (req:Request,res:Response) {
    res.status(404).json({'message':'ERROR 404'});
})