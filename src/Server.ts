import {Request, Response} from "express";
import express from 'express';

const jwt = require('jsonwebtoken');

export const app = express();

export default class Server {
    static port:number = 8090;
    constructor() {
        app.use(function (req: Request, res: Response, next: any) {
            res.setHeader("Access-Control-Allow-Origin", "*");
            res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type, Accept");
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
            next();
        })
            .listen(Server.port, function () {
                console.log('Listening on port '+ Server.port);
            });
    }

    static getDecodedToken(req: Request, res: Response) {
        try {
            if (req.headers.authorization == undefined) {
                throw new Error('Undefined token');
            }
            const token: string = req.headers.authorization.split(' ')[1];
            return jwt.verify(token, 'RANDOM_TOKEN_SECRET');
        } catch (error) {
            return res.status(500).json({error: error.message});
        }
    }
}