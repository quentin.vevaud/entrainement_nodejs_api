import {Request, Response} from "express";
import Server from "../Server";
import UserModel from "../models/UserModel";
import SubscriptionModel from "../models/SubscriptionModel";
import ConfigModel from "../models/ConfigModel";


export default class UserController {

    static profile(req:Request, res:Response) {
        const token = Server.getDecodedToken(req, res);
        if(token.isAdmin) {
            return res.status(401).json({error:"You don't have the rights to access to this fonctionnality"});
        }
        SubscriptionModel.findUserSubscription(token.hostId).then(function (result:any) {
            const subscription = result;
            UserModel.findUserById(token.hostId).then(function (user:any) {
                res.status(200).json({
                    mail : user.mail,
                    firstName : user.firstName,
                    lastName : user.lastName,
                    subscription
                });
            }).catch(function (error:any) {
                res.status(500).json({error : error.message});
            })
        }).catch(function (error:any) {
            res.status(500).json({error : error.message});
        })
    }


    static addToCart(req:Request, res:Response) {
        const token = Server.getDecodedToken(req,res);
        if(token.isAdmin) {
            return res.status(401).json({error:"You don't have the rights to access to this fonctionnality"});
        }
        UserModel.findUserById(token.hostId)
            .then(function (user:any) {
                const subscription:string = req.body.subscription;
                if(subscription != 'monthly' && subscription != 'annual') {
                    return res.status(400).json({error: 'Incorrect request'});
                }
                if(user.cart != undefined) {
                    return res.status(403).json({error: 'There is already a subscription in your cart'});
                }
                const id = user._id;
                SubscriptionModel.findUserSubscription(id)
                    .then(function (result:any) {
                        if(result == null) {
                            UserModel.updateCart(id, subscription)
                                .then(function () {
                                    res.status(201).json({message : subscription +' has correctly been added to the cart'});
                                })
                                .catch(function (error:any) {
                                    res.status(500).json({error : error.message});
                                });
                        } else {
                            res.status(403).json({error:'You already have a subscription.'})
                        }
                    })
                    .catch(function (error:any) {
                        res.status(500).json({error : error.message});
                    })
            })
    }


    static showCart(req:Request, res:Response) {
        const token = Server.getDecodedToken(req,res);
        if(token.isAdmin) {
            return res.status(401).json({error:"You don't have the rights to access to this fonctionnality"});
        }
        UserModel.findUserById(token.hostId).then(function (user:any) {
            if(user.cart == undefined) {
                return res.status(200).json({message : 'Your cart is empty'});
            }
            res.status(200).json({message : user.cart + ' subscription is in your cart'});
        })
    }

    static checkout(req:Request, res:Response) {
        const token = Server.getDecodedToken(req,res);
        if(token.isAdmin) {
            return res.status(401).json({error:"You don't have the rights to access to this fonctionnality"});
        }
        UserModel.findUserById(token.hostId)
            .then(function (user:any) {
                if(user.cart == undefined) {
                    return res.status(400).json({error : 'Your cart is empty'});
                }
                ConfigModel.find()
                    .then(function (result:any) {
                        let price : number = result.monthlySubscriptionPrice;
                        let ttl : string = '31d';
                        if(user.cart == 'annual') {
                            ttl = '365d';
                            price = result.annualSubscriptionPrice;
                        }
                        if(req.body.cardNumber && req.body.name && req.body.expirationDate && req.body.code && req.body.cardNumber.length == 16 && req.body.code.length == 3 && req.body.expirationDate.length == 7) {
                            const expiration = Date.parse(req.body.expirationDate) + 5256000;
                            if(expiration < Date.now()) {
                                return res.status(500).json({error:'Your card is expired'});
                            }
                            SubscriptionModel.createSubscription(ttl,user.cart,price,user._id)
                                .then(function () {
                                    UserModel.updateCart(user._id, undefined)
                                        .then(function () {
                                            res.status(201).json({message : 'Payment success', price});
                                        })
                                        .catch(function (error:any) {
                                            res.status(500).json({error:error.message});
                                        })
                                })
                                .catch(function (error:any) {
                                    res.status(500).json({error : error.message});
                                });
                        } else {
                            return res.status(400).json({error : 'Bad request'});
                        }
                    })
        })
            .catch(function (error) {
                res.status(500).json({error:error.message});
            });
    }
}

