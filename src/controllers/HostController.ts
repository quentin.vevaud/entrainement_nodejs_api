import UserModel from '../models/UserModel';
import {Request, Response} from "express";
import AdminModel from "../models/AdminModel";
import bcrypt from "bcrypt";
import jwt from 'jsonwebtoken';
import ConfigModel from "../models/ConfigModel";
import Server from "../Server";

export default class HostController {

    static login(req: Request, res: Response) {
        UserModel.findUserByMail(req.body.mail)
            .then(function (user: any) {
                if (user == null) {
                    AdminModel.findAdminByMail(req.body.mail)
                        .then(function (admin: any) {
                            if (admin == null) {
                                return res.status(400).json({error:"This user doesn't exist"});
                            }
                            admin.isAdmin = true;
                            action(admin);
                        })
                        .catch(function (error: any) {
                            res.status(500).json({error: error.message});
                        });
                } else {
                    user.isAdmin = false;
                    action(user);
                }
            }).catch(function (error: any) {
            res.status(500).json({error: error.message});
        });

        const action = function (result: any) {
            bcrypt.compare(req.body.password, result.password)
                .then(function (valid: any) {
                    if (!valid) {
                        return res.status(401).json({error: 'Incorrect password !'});
                    }
                    res.status(200).json({
                        token: jwt.sign({
                            hostId: result.id,
                            isAdmin: result.isAdmin
                        }, 'RANDOM_TOKEN_SECRET', {expiresIn: '12h'})
                    });
                })
                .catch(function (error: any) {
                    res.status(500).json({error})
                })
        }
    }


    static logout(req: Request, res: Response) {
        try {
            const token: string = String(req.headers.authorization?.split(' ')[1]);
            ConfigModel.addToken(token)
                .then(function () {
                    res.status(200).json({message: 'Successful logout'});
                })
                .catch(function (error: any) {
                    res.status(500).json({error: error.message});
                });
        } catch (error) {
            res.status(500).json({error: error.message})
        }
    }


    static showSubscriptions(req: Request, res: Response) {
        ConfigModel.find()
            .then(function (result: any) {
                res.status(200).json([
                    {
                        title: 'Monthly subscription',
                        price: result.monthlySubscriptionPrice,
                    },
                    {
                        title: 'Annual subscription',
                        price: result.annualSubscriptionPrice,
                    }
                ])
            })
            .catch(function (error: any) {
                res.status(500).json({error: error.message});
            })
    }


    static auth(req: Request, res: Response, next: any) {
        const token: string = String(req.headers.authorization?.split(' ')[1]);
        ConfigModel.find()
            .then(function (result:any) {
                if (result.tokenBlackList && result.tokenBlackList.includes(token)) {
                    return res.status(500).json({error: 'Invalid token'});
                }
                else {
                    const id = Server.getDecodedToken(req, res).hostId;
                    const isAdmin = Server.getDecodedToken(req, res).isAdmin;
                    if(isAdmin) {
                        AdminModel.findAdminById(id)
                            .then(function (admin: any) {
                                if (admin === null) {
                                    return res.status(400).json({error : "Invalid token"});
                                }
                                next();
                            })
                            .catch(function (error: any) {
                                res.status(500).json({error: error.message});
                            });
                    } else {
                        UserModel.findUserById(id)
                            .then(function (user: any) {
                                if (user == null) {
                                    return res.status(400).json("Invalid token");
                                }
                                next();
                            })
                            .catch(function (error: any) {
                                res.status(500).json({error: error.message});
                            });
                    }
                }
            })
            .catch(function (error:any) {
                res.status(500).json({error:error.message})
            })
    }


    static changeMail(req:Request, res:Response) {
        const token = Server.getDecodedToken(req, res);
        HostController.mailAlreadyExist(req.body.newMail)
            .then(function (boolean:any) {
                if(boolean) {
                    return res.status(403).json({error : 'This mail is already used'})
                }
                if(token.isAdmin) {
                    AdminModel.findAdminById(token.hostId)
                        .then(function (admin:any) {
                            regexVerification();
                            if(req.body.mail == admin.mail) {
                                AdminModel.updateMail(admin.mail, req.body.newMail)
                                    .then(function () {
                                        res.status(200).json({'message' : 'Your mail has been successfully changed'});
                                    })
                                    .catch(function (error:any) {
                                        res.status(500).json({error : error.message});
                                    })
                            } else {
                                res.status(403).json({error : 'Incorrect mail confirmation'});
                            }
                        })
                        .catch(function (error) {
                            res.status(500).json({error:error.message});
                        })
                } else {
                    UserModel.findUserById(token.hostId)
                        .then(function (user:any) {
                            regexVerification();
                            if(req.body.mail == user.mail) {
                                UserModel.updateMail(user.mail, req.body.newMail)
                                    .then(function () {
                                        res.status(200).json({'message' : 'Your mail has been successfully changed'});
                                    })
                                    .catch(function (error:any) {
                                        res.status(500).json({error : error.message});
                                    })
                            } else {
                                res.status(403).json({error : 'Incorrect mail confirmation'});
                            }
                        })
                        .catch(function (error) {
                            res.status(500).json({error:error.message});
                        })
                }
                })
                .catch(function (error:any) {
                    res.status(500).json({error:error.message});
                })
        const regexVerification = function () {
            const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(!regex.test(req.body.newMail)) {
                return res.status(403).json({error:'The format of this mail address is invalid'})
            }
        }
    }

    static async mailAlreadyExist(mail:string) {
        return new Promise(function (resolve, reject) {
           UserModel.findUserByMail(mail)
               .then(function (user:any) {
                   if(user == null) {
                       AdminModel.findAdminByMail(mail)
                           .then(function (admin:any) {
                               if(admin == null) {
                                   return resolve(false);
                               } else {
                                   return resolve(true);
                               }
                           })
                           .catch(function (error:any) {
                               return reject(error)
                           })
                   } else {
                       return resolve(true);
                   }
               })
               .catch(function (error:any) {
                    return reject(error);
               })
       })
    }
}