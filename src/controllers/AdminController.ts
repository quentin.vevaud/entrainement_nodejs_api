import Server from "../Server";
import {Request,Response} from "express";
import bcrypt from "bcrypt";
import HostController from "./HostController";
import UserModel from "../models/UserModel";
import ConfigModel from "../models/ConfigModel";

export default class AdminController {

    static addUser(req:Request, res:Response) {
        const token = Server.getDecodedToken(req, res);
        if(!token.isAdmin) {
            return res.status(401).json({error:"You don't have the rights to access to this functionality"});
        }
        const mail:string = req.body.mail;
        const firstName:string = req.body.firstName;
        const lastName:string = req.body.lastName;
        const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!regex.test(mail)) {
            return res.status(403).json({error:'The format of this mail address is invalid'});
        }
        if(mail === undefined||firstName === undefined||lastName === undefined||req.body.password === undefined) {
            return res.status(400).json({error : 'Invalid request'});
        }
        HostController.mailAlreadyExist(mail).then(function (result:any) {
            if(!result) {
                bcrypt.hash(req.body.password, 10)
                    .then(function (password: string) {
                        UserModel.addUser(mail,password, firstName,lastName)
                            .then(function () {
                                res.status(201).json({'message': 'User has been successfully added'});
                            })
                            .catch(function (error: any) {
                                res.status(500).json({error : error.message});
                            });
                    })
                    .catch(function (error: any) {
                        res.status(500).json({error : error.message});
                    });
            } else {
                res.status(403).json({error: 'Email already used'})
            }
        })
    }

    static changePrice(req:Request, res:Response) {
        const token = Server.getDecodedToken(req,res);
        if(!token.isAdmin) {
            return res.status(401).json({error:"You don't have the rights to access to this functionality"})
        }
        let booleanConversion:boolean;
        const isAnnual : string = req.body.isAnnual;
        if(isAnnual == 'false') {
            booleanConversion = false;
        } else if (isAnnual == 'true') {
            booleanConversion = true;
        } else {
            return res.status(400).json({error:'Invalid request'});
        }
        ConfigModel.changePrice(req.body.price,booleanConversion).then(function () {
            res.status(200).json({'message':'Price changed'});
        }).catch(function (error:any) {
            res.status(500).json({error:error.message});
        })
    }
}