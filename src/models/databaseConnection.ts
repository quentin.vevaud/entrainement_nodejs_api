const mongoose = require('mongoose')

export default class DatabaseConnection {
    constructor() {
        mongoose.connect("mongodb+srv://root:VzW_dn6hVU@HQAC@apidatabase.ri1xu.mongodb.net/apiDatabase?retryWrites=true&w=majority",
            { useNewUrlParser: true,
                useUnifiedTopology: true })
            .then(() => console.log('Connexion à MongoDB réussie !'))
            .catch((error:any) => console.log('Connexion à MongoDB échouée !'+ error));
    }
}