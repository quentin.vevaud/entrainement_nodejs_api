import UserSchema from "../Schemas/UserSchema";

export default class UserModel {

    static async findUserByMail(mail:string) {
        return await UserSchema.findOne({mail}).exec();
    }

    static async findUserById(id:string) {
        return await UserSchema.findById({_id : id}).exec();
    }

    static async updateCart(id:string, cart:string|undefined) {
        await UserSchema.updateOne({_id : id},{cart}).exec();
    }

    static async updateMail(mail:string, newMail:string) {
        await UserSchema.updateOne({mail},{mail:newMail}).exec();
    }

    static async addUser(mail:string, password:string, firstName:string, lastName:string) {
        await new UserSchema({
            mail,
            password,
            firstName,
            lastName,
            cart : undefined
        }).save()
    }
}