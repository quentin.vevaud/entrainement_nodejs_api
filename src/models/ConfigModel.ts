import ConfigSchema from "../Schemas/ConfigSchema";

export default class ConfigModel {

    private static id : string = '5f1ed838d2079089a0fe6d97';

    static async find() {
        return await ConfigSchema.findById({_id : this.id}).then();
    }
    static async addToken(token:string) {
        return await ConfigModel.find().then(function (result:any) {
            const blackList:string[] = result.tokenBlackList;
            blackList.push(token);
            ConfigSchema.updateOne({_id : '5f1ed838d2079089a0fe6d97'},{tokenBlackList:blackList}).exec();
        })
    }
    static async changePrice(price:number, isAnnual:boolean) {
        if(isAnnual) {
            return await ConfigSchema.updateOne({_id : this.id}, {annualSubscriptionPrice : price}).exec();
        } else {
            return await ConfigSchema.updateOne({_id : this.id}, {monthlySubscriptionPrice : price}).exec();
        }
    }
}