import SubscriptionSchema from "../Schemas/SubscriptionSchema";

export default class SubscriptionModel {

    static async findUserSubscription(idUser:string) {
        return await SubscriptionSchema.findOne({idUser}).exec();
    }
    static async createSubscription(ttl:string, subscription:string, price:number, idUser:string) {
        const subscriptionObject = new SubscriptionSchema({
            subscription,
            price,
            idUser
        });
        subscriptionObject.ttl = ttl;
        await subscriptionObject.save();
    }
}