import AdminSchema from "../Schemas/AdminSchema";

export default class AdminModel {

    static async findAdminByMail(mail:string) {
        return await AdminSchema.findOne({mail}).exec();
    }
    static async findAdminById(id:string) {
        return await AdminSchema.findById({_id:id}).exec();
    }
    static async updateMail(mail:string, newMail:string) {
        await AdminSchema.updateOne({mail},{mail:newMail}).exec();
    }
    static async createAdmin(mail:string, password:string) {
        await new AdminSchema({mail,password}).save();
    }
}